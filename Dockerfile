FROM hypriot/rpi-node:latest

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Pull latest code from gitlab
RUN wget https://gitlab.com/RF24-MQTT/RF24SN_nodejs_Server/repository/archive.tar.bz2
RUN tar -xf archive.tar.bz2 --strip-components 1
RUN npm install --global

ENTRYPOINT ["rf24sn"] 
CMD ["-b", "mqtt://192.168.178.44:1883", "-spi", "/dev/spidev0.0", "-ce", "25", "-irq", "24", "-vvvv"]